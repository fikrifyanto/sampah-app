## Installation
- Import db.sql to your database
- Set your host name, database name, username, password and default controller and method in config/config.php
- Set variable :
    * BASE_URL
    * DEFAULT_CONTROLLER
    * DEFAULT_METHOD
    * DB_HOST
    * DB_DATABASE
    * DB_USERNAME
    * DB_PASSWORD
- Done

## API
- /api/materials
- /api/types

## Demo
- [https://sampah-app.000webhostapp.com/](https://sampah-app.000webhostapp.com/)
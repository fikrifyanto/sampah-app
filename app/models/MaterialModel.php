<?php

class MaterialModel extends Model
{
    public function table()
    {
        $this->table = 'material';
    }

    public function materialID($name)
    {
        $material = "";
        $code = sprintf("%'.03d", $this->count()+1);
        $name = str_split(strtoupper($name));
        $consonant = ["A", "I", "U", "E", "O"];
        foreach ($name as $sub) {
            if (!in_array($sub, $consonant)) {
                $material = $material.$sub;
            }
        }
        return "MAT-$code-$material";
    }

    public function store($data)
    {
        $data['id'] = $this->materialID($data['name']);
        $query = "INSERT INTO $this->table VALUES (:id, :name)";
        $this->db->query($query);
        $this->db->bind('id', $data['id']);
        $this->db->bind('name', $data['name']);
        $this->db->execute();

        return $this->db->rowCount();
    }
}

<?php

class TypeModel extends Model
{
    public function table()
    {
        $this->table = 'type';
    }
    
    public function typeID($name)
    {
        $type = "";
        $code = sprintf("%'.03d", $this->count()+1);
        $name = str_split(strtoupper($name));
        $consonant = ["A", "I", "U", "E", "O"];
        foreach ($name as $sub) {
            if (!in_array($sub, $consonant)) {
                $type = $type.$sub;
            }
        }
        return "TYP-$code-$type";
    }

    public function store($data)
    {
        $data['id'] = $this->typeID($data['name']);
        $query = "INSERT INTO $this->table VALUES (:id, :material_id, :name)";
        $this->db->query($query);
        $this->db->bind('id', $data['id']);
        $this->db->bind('material_id', $data['material_id']);
        $this->db->bind('name', $data['name']);
        $this->db->execute();

        return $this->db->rowCount();
    }
}

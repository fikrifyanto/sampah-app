    <div class="headbar">
        <p>Data Sampah</p>    
    </div>
    <div class="container">
        <a href="<?= BASE_URL ?>/type/create"><button class="btn btn-primary btn-lg btn-block mb-4">Tambah</button></a>

        <?php foreach ($data['types'] as $type) : ?>
        <div class="card rounded-lg mt-2">
            <div class="card-header">
                <h4><?= $type['name'] ?></h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col"><?= $type['material_name'] ?></div>
                    <div class="col text-right"><a href="<?= BASE_URL ?>/type/destroy/<?= $type['id'] ?>" class="text-danger">Hapus</a></div>
                </div>
            </div>
        </div>
        <?php endforeach ?>
        
    </div>
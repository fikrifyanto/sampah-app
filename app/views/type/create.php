    
    <div class="headbar">
        <p>Input Sampah</p>    
    </div>

    <div class="container">
        <form action="<?= BASE_URL ?>/type/store" method="POST">
            <div class="form-group">
                <label for="type">Kategori Sampah</label>
                <select name="material_id" id="material_id" class="form-control" required>
                    <option value="" disabled selected hidden>Kategori</option>
                    <?php foreach($data['materials'] as $material) : ?>
                        <option value="<?= $material['id'] ?>"><?= $material['name'] ?></option>
                    <?php endforeach ?>
                    <option value="__create">Tambah Kategori +</option>
                </select>
            </div>
            <div class="form-group">
                <label for="type">Nama Sampah</label>
                <input type="text" id="name" name="name" class="form-control" placeholder="Nama Sampah" required>
            </div>
            <button class="btn btn-primary btn-lg btn-block mb-4">Simpan</button>
        </form>
    </div>

    <script>
        document.getElementById("material_id").addEventListener("change", function () {
            if(this.value == "__create") {
                location.href = "<?= BASE_URL ?>/material/create";
            }
        });
    </script>
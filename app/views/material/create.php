    
    <div class="headbar">
        <p>Input Kategori Sampah</p>    
    </div>

    <div class="container">
        <form action="<?= BASE_URL ?>/material/store" method="POST">
            <div class="form-group">
                <label for="name">Nama Kategori Sampah</label>
                <input type="text" id="name" name="name" class="form-control" placeholder="Nama Kategori Sampah" required>
            </div>
            <button class="btn btn-primary btn-lg btn-block mb-4">Simpan</button>
        </form>
    </div>
<?php

class Api extends Controller
{
    public function materials()
    {
        $data['materials'] = $this->model('MaterialModel')->all();
        $this->json($data);
    }
    public function types()
    {
        $data['types'] = $this->model('TypeModel')->all();
        $this->json($data);
    }
}

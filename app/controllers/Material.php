<?php

class Material extends Controller
{
    public function __construct()
    {
        $this->material = $this->model("MaterialModel");
    }

    public function index()
    {
        $data['materials'] = $this->model('MaterialModel')->all();
        $this->view('layout/header');
        $this->view('material/index', $data);
        $this->view('layout/footer');
    }

    public function create()
    {
        $this->view('layout/header');
        $this->view('material/create');
        $this->view('layout/footer');
    }

    public function store()
    {
        $store = $this->material->store($_POST);
        if ($store > 0) {
            exit(header("Location: ".BASE_URL."/type/create"));
        }
    }
}

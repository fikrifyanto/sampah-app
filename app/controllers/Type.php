<?php

class Type extends Controller
{
    public function __construct()
    {
        $this->type = $this->model("TypeModel");
        $this->material = $this->model("MaterialModel");
    }

    public function index()
    {
        $data['types'] = $this->type->join(
            "type.id, type.name, material.name as material_name",
            "material",
            "material_id",
            "id"
        );
        $data['materials'] = $this->material->all();
        $this->view('layout/header');
        $this->view('type/index', $data);
        $this->view('layout/footer');
    }

    public function create()
    {
        $data['materials'] = $this->material->all();
        $this->view('layout/header');
        $this->view('type/create', $data);
        $this->view('layout/footer');
    }

    public function store()
    {
        $store = $this->type->store($_POST);
        if ($store > 0) {
            exit(header("Location: ".BASE_URL."/type/index"));
        }
    }

    public function destroy($id)
    {
        $store = $this->type->destroy($id);
        if ($store > 0) {
            exit(header("Location: ".BASE_URL."/type/index"));
        }
    }
}
